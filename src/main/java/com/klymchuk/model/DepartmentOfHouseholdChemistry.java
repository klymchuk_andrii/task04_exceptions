package com.klymchuk.model;

import com.klymchuk.products.CleaningProducts;
import com.klymchuk.products.CleaningProductsType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class DepartmentOfHouseholdChemistry implements Model {
    List<CleaningProducts> cleaningProducts;

    public DepartmentOfHouseholdChemistry() {
        cleaningProducts = new ArrayList<>();
    }

    @Override
    public List<CleaningProducts> getCleaningProductsList() {
        return cleaningProducts;
    }

    @Override
    public List<CleaningProducts> getListOfSelectedCleaningProducts(
            ArrayList<String> cleaningProducts) {
        List<CleaningProducts> selectedCleaningProducts = new ArrayList<>();
        for (String s : cleaningProducts) {
            selectedCleaningProducts.add(new CleaningProducts(
                    CleaningProductsType.valueOf(s)));
        }
        selectedCleaningProducts.sort(Comparator.comparingInt(
                CleaningProducts::getPrice));
        return selectedCleaningProducts;
    }

    @Override
    public void getCleaningProductsFromFile() throws IOException {
        File file = new File("CleaningProducts.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;

        while ((line = reader.readLine()) != null) {
            cleaningProducts.add(new CleaningProducts
                    (CleaningProductsType.valueOf(line)));
        }
        reader.close();
    }

    @Override
    public void addCleaningProduct(String cleaningProduct) {
        cleaningProducts.add(new CleaningProducts(CleaningProductsType.valueOf(cleaningProduct)));
    }
}
