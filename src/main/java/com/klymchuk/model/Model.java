package com.klymchuk.model;

import com.klymchuk.products.CleaningProducts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface Model {
    List<CleaningProducts> getCleaningProductsList();

    List<CleaningProducts> getListOfSelectedCleaningProducts(
            ArrayList<String> cleaningProducts);

    void getCleaningProductsFromFile() throws IOException;

    void addCleaningProduct(String cleaningProduct);
}
