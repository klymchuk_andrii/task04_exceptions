package com.klymchuk.controller;


import com.klymchuk.products.CleaningProductsType;

import java.io.IOException;

public interface Controller {
    void printCleaningProductsList() throws IOException;

    void printListOfSelectedItem(final String nameOfProducts);

    void addCleaningProduct(String type);

    void getFromFile() throws IOException;
}
