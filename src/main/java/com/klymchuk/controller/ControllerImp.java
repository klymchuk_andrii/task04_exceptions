package com.klymchuk.controller;

import com.klymchuk.model.DepartmentOfHouseholdChemistry;
import com.klymchuk.model.Model;
import com.klymchuk.products.CleaningProductsType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ControllerImp implements Controller{
    private Model model;

    public ControllerImp(){
        model = new DepartmentOfHouseholdChemistry();
    }

    @Override
    public void getFromFile() throws IOException{
        model.getCleaningProductsFromFile();
    }

    @Override
    public void printCleaningProductsList(){
        System.out.println(model.getCleaningProductsList());
    }

    @Override
    public void printListOfSelectedItem(String nameOfProducts)throws NullPointerException{
        ArrayList<String> products = new ArrayList<>();
        String[] strings = nameOfProducts.split(" ");
        for (String s:strings) {
            products.add(s);
        }
        System.out.println(model.getListOfSelectedCleaningProducts(products));
    }

    @Override
    public void addCleaningProduct(String type){
        model.addCleaningProduct(type);
    }
}
