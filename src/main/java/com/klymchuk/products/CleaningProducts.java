package com.klymchuk.products;

public class CleaningProducts {
    CleaningProductsType type;
    int price;

    public CleaningProducts(){
        price=0;
    }

    public CleaningProducts(CleaningProductsType typeParam){
        type = typeParam;
        price = typeParam.getPrice();
    }

    public int getPrice() {
        return price;
    }


    @Override
    public String toString() {
        return "CleaningProducts{" +
                "type=" + type +
                ", price=" + price +
                '}';
    }
}
