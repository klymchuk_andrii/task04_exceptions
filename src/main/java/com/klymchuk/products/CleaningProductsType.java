package com.klymchuk.products;

public enum CleaningProductsType {
    BROOM {
        public int price = 25;

        public int getPrice() {
            return price;
        }
    }, RAG {
        public int price = 10;

        public int getPrice() {
            return price;
        }
    }, DETERGENT {
        public int price = 38;

        public int getPrice() {
            return price;
        }
    };

    public int getPrice() {
        throw new RuntimeException("Can`t find getPrice() method of this flower`s type!");
    }
}
