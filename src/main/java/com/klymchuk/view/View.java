package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import com.klymchuk.controller.ControllerImp;
import com.klymchuk.products.CleaningProductsType;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class View {
    private static Controller controller;
    private static Scanner input;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    static {
        input = new Scanner(System.in);
        controller = new ControllerImp();
    }

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print list of all cleaning products");
        menu.put("2", "  2 - print selected cleaning products");
        menu.put("3", "  3 - add cleaning product");
        menu.put("4", "  4 - read from file");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printCleaningProducts);
        methodsMenu.put("2", this::selectCleaningProducts);
        methodsMenu.put("3", this::addCleaningProducts);
        methodsMenu.put("4", this::readFromFile);
    }

    private void readFromFile() {
        try {
            controller.getFromFile();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private void addCleaningProducts() {
        System.out.print("Please enter products type: ");
        String userString = null;
        int countOfEnum = 0;

        try {
            userString = input.nextLine();
            if (!userString.matches("^[A-Z]+")) throw new NumberFormatException();
            Pattern pattern;
            Matcher matcher;
            pattern = Pattern.compile("^[A-Z]+");
            matcher = pattern.matcher(userString);

            String type = null;
            if (matcher.find()) {
                type = (matcher.group());
            } else throw new NullPointerException();

            for (CleaningProductsType c : CleaningProductsType.values()) {
                if (!type.equals(c)) {
                    countOfEnum++;
                }
            }
            if (countOfEnum == 0) {
                throw new UnavailableCleaningProduct();
            }

            controller.addCleaningProduct(type);
        } catch (NumberFormatException e) {
            System.out.println("Wrong input! Try again");
        } catch (NullPointerException e) {
            System.out.println(e);
        } catch (UnavailableCleaningProduct unavailableFlower) {
            unavailableFlower.printStackTrace();
        }

    }

    public void printCleaningProducts() {
        try {
            controller.printCleaningProductsList();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void selectCleaningProducts() {
        try {
            String cleaningProducts = null;
            cleaningProducts = input.nextLine();
            controller.printListOfSelectedItem(cleaningProducts);
        } catch (NullPointerException e) {
            System.out.println(e);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");

            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}
